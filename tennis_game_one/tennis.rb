require_relative './player.rb'

class TennisGame1

  def initialize(player1Name, player2Name)
    @player1 = Player.new(player1Name)
    @player2 = Player.new(player2Name)
  end
        
  def won_point(playerName)
    @player1.add_score_if_me(playerName)
    @player2.add_score_if_me(playerName)
  end
  
  def score
    result = ""
    player_1_points = @player1.score
    player_2_points = @player2.score
    tempScore=0
    if (player_1_points==player_2_points)
      result = {
          0 => "Love-All",
          1 => "Fifteen-All",
          2 => "Thirty-All",
      }.fetch(player_1_points, "Deuce")
    elsif (player_1_points>=4 or player_2_points>=4)
      minusResult = player_1_points-player_2_points
      if (minusResult==1)
        result ="Advantage player1"
      elsif (minusResult ==-1)
        result ="Advantage player2"
      elsif (minusResult>=2)
        result = "Win for player1"
      else
        result ="Win for player2"
      end
    else
      (1...3).each do |i|
        if (i==1)
          tempScore = player_1_points
        else
          result+="-"
          tempScore = player_2_points
        end
        result += {
            0 => "Love",
            1 => "Fifteen",
            2 => "Thirty",
            3 => "Forty",
        }[tempScore]
      end
    end
    result
  end
end
