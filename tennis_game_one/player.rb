class Player 
  def initialize(name)
    @name = String(name)
    @score = 0
  end

  def name
    name
  end

  def score
    @score
  end

  def add_score_if_me(name)
    if name == @name
      @score += 1
    end
  end
end